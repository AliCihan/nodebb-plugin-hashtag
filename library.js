"use strict";

const slugify = require.main.require("./src/slugify");

var controllers = require("./lib/controllers"),
  regexTwitter = require("./lib/twitterRegexHashtag"),
  nconf = require.main.require("nconf"),
  topics = require.main.require("./src/topics"),
  meta = require.main.require("./src/meta"),
  helpersRoute = require.main.require("./src/routes/helpers"),
  websockets = require.main.require("./src/socket.io"),
  plugin = {
    templates: {},
  };

const HashRegex = regexTwitter;

const isLatinMention = /[\w\d\-_.]+$/;

let app;

plugin.init = async (params) => {
  app = params.app;
  helpersRoute.setupAdminPageRoute(
    params.router,
    "/admin/plugins/connect-hashtag",
    [],
    controllers.renderAdminPage
  );
};

plugin.defineWidgets = async (widgets) => {
  widgets = widgets.concat([
    {
      widget: "hashtagscategory",
      name: "Hashtags Category",
      description: "Render Hashtags Category",
      content: "widgets/hashtagscategorywidget.tpl",
    },
  ]);
  return widgets;
};

plugin.renderHashtagsCategoryWidget = async (widget) => {
  var cidMatch = widget.area.url.match("category/([0-9]+)");
  if (cidMatch) {
    var cid = cidMatch.length > 1 ? cidMatch[1] : 1;

    const items = await topics.getCategoryTagsData([cid], 0, 10);

    const output = await app.renderAsync("widgets/hashtagscategorywidget.tpl", {
      items: items,
      relative_path: nconf.get("relative_path"),
    });

    widget.html = output;
    return widget;
  }
  return;
};

plugin.addAdminNavigation = async (header) => {
  header.plugins.push({
    route: "/plugins/connect-hashtag",
    icon: "fa-tint",
    name: "Hashtag",
  });

  return header;
};

plugin.addTagsToTopic = async function (postData) {
  if (!postData || !postData.post) {
    return;
  }

  var content = postData.post.content;

  var matches = content.match(HashRegex);

  if (!matches) {
    return;
  }

  matches = matches.map(function (match) {
    return slugify(match);
  });

  console.log(matches);

  if (!matches.length) {
    return;
  }

  const currentTags = await topics.getTopicTags(postData.post.tid);

  await topics.createTags(matches, postData.post.tid, postData.post.timestamp);
  await topics.updateTopicTags(postData.post.tid, [...matches, ...currentTags]);
  let result = await topics.getTopicTagsObjects(postData.post.tid);
  websockets
    .in(`topic_${postData.post.tid}`)
    .emit("plugin_hashtag_event:hashtag_edit", {
      tid: postData.post.tid,
      tags: result,
    });
  return;
};

plugin.editTagsToTopic = async function (postData) {
  if (!postData || !postData.post) {
    return;
  }

  if (postData.post.changed) {
    var newContentTag = postData.post.content.match(HashRegex);
    if (newContentTag) {
      newContentTag = newContentTag.map(function (match) {
        return slugify(match);
      });
    } else {
      newContentTag = [];
    }
    var oldContentTag = postData.post.oldContent.match(HashRegex);
    if (oldContentTag) {
      oldContentTag = oldContentTag.map(function (match) {
        return slugify(match);
      });
    } else {
      oldContentTag = [];
    }
    const differenceAB_added = newContentTag.filter(
      (x) => oldContentTag.indexOf(x) === -1
    );
    const differenceBA_deleted = oldContentTag.filter(
      (x) => newContentTag.indexOf(x) === -1
    );

    let currentTags = await topics.getTopicTags(postData.post.tid);

    currentTags = currentTags.filter(
      (el) => !differenceBA_deleted.includes(el)
    );

    if (differenceAB_added.length > 0 || differenceBA_deleted.length > 0) {
      await topics.updateTopicTags(postData.post.tid, [
        ...currentTags,
        ...differenceAB_added,
      ]);
      let result = await topics.getTopicTagsObjects(postData.post.tid);
      websockets
        .in(`topic_${postData.post.tid}`)
        .emit("plugin_hashtag_event:hashtag_edit", {
          tid: postData.post.tid,
          tags: result,
        });
    }
  }
  return;
};

plugin.checkMaxHashtags = async (data) => {
  if (
    !data ||
    !data.post ||
    data.post.content === null ||
    data.post.content === undefined
  ) {
    return data;
  }
  var content = data.post.content;

  var matches = content.match(HashRegex);

  if (!matches) {
    return data;
  }

  let unique_matches = [...new Set(matches)];

  const pluginData = await meta.settings.get("connect-hashtag");

  if (pluginData && pluginData.max && !isNaN(pluginData.max)) {
    if (unique_matches.length > pluginData.max) {
      throw new Error(
        "[[connect_hashtag:too-many-hashtags," + pluginData.max + "]]"
      );
    }
  }

  return data;
};

plugin.parsePost = async (data) => {
  if (!data || !data.postData || !data.postData.content) {
    return data;
  }

  const content = await plugin.parseRaw(data.postData.content);
  data.postData.content = content;
  return data;
};

plugin.parseRaw = async (content) => {
  const pluginData = await meta.settings.get("connect-hashtag");

  if (!pluginData) {
    pluginData = {};
  }

  var matches = content.match(HashRegex);
  if (!matches) {
    return content;
  }
  let unique_matches = [...new Set(matches)];

  unique_matches.forEach((match) => {
    let sluggedMatch = slugify(match).trim();

    const _regex = isLatinMention.test(sluggedMatch)
      ? new RegExp(`(?:^|\\s|\>|;)#${sluggedMatch}\\b`, "gi")
      : new RegExp(`(?:^|\\s|\>|;)#${sluggedMatch}`, "gi");

    let str = ` <a class="plugin-hashtag-a" href="${nconf.get(
      "url"
    )}/tags/${sluggedMatch}">#${sluggedMatch}</a> `;

    content = content.replace(_regex, str);
  });

  return content;
};

plugin.rerouteTags = async (data) => {
  const pluginData = await meta.settings.get("connect-hashtag");
  if (pluginData && pluginData.route_path) {
    data.mounts["tags"] = pluginData.route_path.replace("/", "");
  }
  return data;
};

module.exports = plugin;
