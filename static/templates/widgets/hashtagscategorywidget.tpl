<!-- IF items.length -->
<div>
  <!-- BEGIN items -->
  <a
    href="{relative_path}/hashtags/{items.value}"
    data-value="{items.value}"
    class="tag-container btn btn-tags"
    ><span class="tag-item" data-tag="{items.value}" style=""
      >{items.value}</span
    ><span class="tag-topic-count human-readable-number" title="{items.score}"
      >{items.score}</span
    ></a
  >
  <!-- END items -->
</div>
<!-- ENDIF items.length -->
