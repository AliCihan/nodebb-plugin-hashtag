<div class="row">
  <form role="form" class="connect-hashtag-settings">
    <div class="col-sm-2 col-xs-12 settings-header">Settings</div>
    <div class="col-sm-10 col-xs-12">
      <div class="form-group">
        <label for="max"
          >Inserire il numero di hashtag massimo per ogni post</label
        >
        <input
          type="text"
          id="max"
          name="max"
          class="form-control"
          placeholder="Max hashtags"
        />
      </div>
      <div class="form-group">
        <label for="route_path"
          >Utilizza la seguente route al posto di "/tags"</label
        >
        <input
          type="text"
          id="route_path"
          name="route_path"
          class="form-control"
          placeholder="/hashtags"
        />
        <p class="help-block">
          Modifica la voce a menu dall'admin<br />
          <a href="{config.relative_path}/admin/general/navigation"
            >General &gt; Navigation</a
          >
        </p>
      </div>
    </div>
  </form>
  <button
    id="save"
    class="floating-button mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"
  >
    <i class="material-icons">save</i>
  </button>
</div>
