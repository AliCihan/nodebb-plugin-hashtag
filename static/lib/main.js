"use strict";

/* globals $, app, socket */

(function () {
  function updateTagsTopic(data) {
    if (data.tags && tagsUpdatedGT(data.tags)) {
      app.parseAndTranslate(
        "partials/topic/tags",
        { tags: data.tags },
        function (html) {
          console.log(html);
          var tags = $(".tags");
          tags.fadeOut(250, function () {
            tags.html(html).fadeIn(250);
          });
        }
      );
    }
  }

  // same function as event.js:128
  function tagsUpdatedGT(tags) {
    if (tags.length !== $(".tags").first().children().length) {
      return true;
    }

    for (var i = 0; i < tags.length; ++i) {
      if (!$('.tags .tag-item[data-tag="' + tags[i].value + '"]').length) {
        return true;
      }
    }
    return false;
  }

  socket.on("plugin_hashtag_event:hashtag_edit", updateTagsTopic);
})();
