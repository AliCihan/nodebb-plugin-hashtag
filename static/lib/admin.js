"use strict";

define("admin/plugins/connect-hashtag", ["settings"], function (Settings) {
  var ACP = {};

  ACP.init = function () {
    Settings.load("connect-hashtag", $(".connect-hashtag-settings"));

    $("#save").on("click", function () {
      var max = $("#max").val();
      var regex_number = new RegExp("^[1-9]+[0-9]*$");
      if (regex_number.test(max)) {
        Settings.save(
          "connect-hashtag",
          $(".connect-hashtag-settings"),
          function () {
            app.alert({
              type: "success",
              alert_id: "connect-hashtag-saved",
              title: "Settings Saved",
              message: "Please reload your NodeBB to apply these settings",
              clickfn: function () {
                socket.emit("admin.reload");
              },
            });
          }
        );
      } else {
        app.alert({
          type: "danger",
          alert_id: "connect-hashtag-error",
          title: "Wrong Input",
          message: "Put only integer value",
        });
      }
    });
  };

  return ACP;
});
